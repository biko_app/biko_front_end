# Installation Instructions

Navigate on terminal to the project's root folder.

### Install node and npm
Download and install from http://nodejs.org/

### Install bower
On terminal run ```sudo npm install -g bower```  
http://bower.io/

### Install grunt
On terminal run ```sudo npm install -g grunt-cli```  
http://gruntjs.com/

### Update everything
On terminal run ```sudo npm install```  
On terminal run ```bower install```  

# Build Instructions

Navigate on terminal to the project's root folder.

On terminal run ```grunt build```  
This will concatenate and minify all assets and put the build inside the ```/bin``` folder. This folder can the be copied to the public folder of the server.

# Development Instructions

```gem install bundler```
Inside static ```bundle install```
```bundle exec compass watch```