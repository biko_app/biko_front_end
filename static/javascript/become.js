;(function(window, $){

	'use strict';
	var Become = function(){

		var labels = {
			'PLACE_HOLDER' : 'Transform this text in something else...'
			},
			page_id = $("#page_id").val(),
			services = {
				'SERVICE_SAVE_CONTENT' : '/api/v1/become/content/save/'
			};

		function savePage(){
			$('.editable-object').each(function(index, element){
				element
				$('.editable-object')

				$.ajax(services.SERVICE_SAVE_CONTENT,{
					'data': JSON.stringify({
						'text' : this.innerHTML,
						'page_id': page_id,
						'position':index+1
					}),
					'type': 'POST',
					'processData': false,
					'contentType': 'application/json'
				});
			});
		}

		function addEvents(){
			$("#hide-menu").on("click", function(event){
				event.preventDefault();
				if($(".site-nav .public-menu").is(":visible")){
					$(".site-nav .public-menu").hide();
				} else {
					$(".site-nav .public-menu").show();
				}
			});

			$("#edit-mode").on("click", function(event){
				event.preventDefault();
				savePage();
			});
		}

		function become(){
			if(!$("#session-id").val()){
				return;
			}
			$('.editable-object').each(function(){
				$(this).attr("data-placeholder", labels.PLACE_HOLDER);
				createEditor(this);
			});
			addEvents();
		}


		function createEditor(element){
			return new wysihtml5.Editor(element, {
				parserRules:  wysihtml5ParserRules
			});
		}

		return new become();
	}();

}(window, jQuery));