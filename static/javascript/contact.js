;(function(window, scope, $){

	'use strict';

	/**
	 * This class contains all specific functionality for the Register
	 */
	scope.Contact = function(){

		var contactMapContainer = $("#map-contact-us"),
			interactiveMap,
			contactForm = $("#contact-form"),
			contactFormNameField = $("#contact-form-name"),
			contactFormEmailField = $("#contact-form-email"),
			contactFormCommentField = $("#contact-form-comment");

		function contact(){
			addEvents();
			initMap();
		}

		function createMaker(){
			return new RichMarker({
				position: new google.maps.LatLng(4.685943, -74.061461),
				map: interactiveMap,
				animation: google.maps.Animation.DROP,
				draggable: false,
				content: '<span class="map-pin"><span class="icon-pin-2"></span></span>'
			});
		}


		function initMap(){
			if(contactMapContainer.size()== 0){
				return;
			}
			contactMapContainer.css({
				"position" : "relative",
				"width" : "100%",
				"height" : "100%"
			});
			var zoom = 17;
			if($(window).width() < 490){
				zoom = 16;
			}
			var options = {
				zoom: zoom,
				disableDefaultUI: true,
				draggable: false,
				scrollwheel: false,
				streetViewControl: false,
				disableDoubleClickZoom: true,
				mapTypeId: google.maps.MapTypeId.ROADMAP,
				center: new google.maps.LatLng(4.684561, -74.061386)
			};
			interactiveMap = new google.maps.Map(contactMapContainer.get(0), options);
			createMaker();
		}


		function addEvents(){
			contactForm.on("submit", function(){
				var errorFound = true
				if($.trim(contactFormNameField.val()) === ""){
					contactFormNameField.parent().find( ".error" ).show();
					errorFound = false;
				} else {
					contactFormNameField.parent().find( ".error" ).hide();
				}

				if($.trim(contactFormEmailField.val()) === ""){
					contactFormEmailField.parent().find( ".error" ).show();
					errorFound = false;
				} else {
					contactFormEmailField.parent().find( ".error" ).hide();
				}

				if($.trim(contactFormCommentField.val()) === ""){
					contactFormCommentField.parent().find( ".error" ).show();
					errorFound = false;
				} else {
					contactFormCommentField.parent().find( ".error" ).hide();
				}
				return errorFound;
			});
		}

		return new contact();
	}();

}(window, window.Biko, jQuery));