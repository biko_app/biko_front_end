;(function(window, $){

	'use strict';
	
	window.Biko = function(){

		var menuIcon = $("#menu-icon"),
			menuMobile = $(".mobile-menu"),
			isiOS = ( navigator.userAgent.match(/(iPad|iPhone|iPod)/g) ? true : false );


		function addEvents(){


			var addWindowEvent = function(){
				var action = function(e){
					var container = $(".mobile-menu");
					if (!container.is(e.target) && container.has(e.target).length === 0 && menuMobile.is(':visible')){
						menuMobile.hide();
					}
				};
				$(document).on("mouseup", action);
				$(document).on("touchstart", action);
			}();

			menuIcon.on("click", function(event){
				event.preventDefault();
				event.stopPropagation();
				menuMobile.css({
					"height" : $(document).height()
				});
				menuMobile.show();
			});

			menuIcon.on("touchstart", function(event){
				event.preventDefault();
				event.stopPropagation();
				menuMobile.css({
					"height" : $(document).height()
				});
				menuMobile.show();
			});
		}


		function createModal(element){
			var modal = $(document.createElement("div")),
				content = element;
			modal.css({
				'position':'fixed',
				'width' : '100%',
				'height' : '100%',
				'top':0,
				'left':0,
				'background':'rgba(0,0,0,.7)',
				'z-index':10000

			});
			content.css({
				'z-index':10001,
				'display' : 'block',
				'position':'fixed',
				'top':'50%',
				'left':'50%',
				'margin': '-'+(content.height()/2) +'px 0 0 -'+(content.width()/2)+'px'
			}).find('.close-action').off('click').on('click', function(event){
				event.preventDefault();
				if(this.id === "facebook"){
					window.Biko.Facebook.share();
				}
				if(this.id === "twitter"){
					window.Biko.Twitter.share();
				}
				content.hide();
				modal.remove();
			});
			$(document.body).append(modal)
			return modal;
		}


		function facebookStatusConnected(response){
			console.log(response);
		}

		function facebookNotAuthorized(response){
			
		}

		function facebookNotFound(response){
			
		}


		(function(){
			addEvents();
			if(isiOS){
				$(document.body).addClass("ios");
			}
		}());


		return {
			'createModal' : createModal,
			'facebookStatusConnected' : facebookStatusConnected,
			'facebookNotAuthorized' : facebookNotAuthorized,
			'facebookNotFound' : facebookNotFound
		};
	}();

}(window, jQuery));