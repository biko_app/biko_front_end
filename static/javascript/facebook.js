;(function(window, scope){

	'use strict';

	/**
	 * This class allow the authentication with facebook
	 */
	scope.Facebook = function(){ 

		var accessToken,
			appId = $("#social_facebook_app_id").val();
		
		/** @constructor */
		function facebook(){
			addEvents();
			loadFacebookScript();
		}

		/**
		 * Loads the facebook script
		 */
		function loadFacebookScript(){
			var script = document.createElement('script');
			script.src = document.location.protocol + '//connect.facebook.net/en_US/all.js';
			script.async = true;
			document.body.appendChild(script);
		}

		/**
		 * Adds all events to the facebook page
		 */
		function addEvents(){
			window.fbAsyncInit = function() {
				initFacebook();
			};
		}



		function initFacebook(){
			/** Calls and initializes facebook */
			FB.init({
				appId: appId,
				oauth: true,
				status: true,
				cookie: true,
				xfbml: true
			});
			FB.getLoginStatus(function(response) {
				statusChangeCallback(response);
			});
		}


		function statusChangeCallback(response) {
			if (response.status === 'connected') {
				accessToken = response.authResponse.accessToken;
				scope.facebookStatusConnected(response);
			} else if (response.status === 'not_authorized') {
				scope.facebookNotAuthorized(response);
			} else {
				scope.facebookNotFound(response);
			}
		}

		function getFacebookSession(){
			FB.api('/me', function(response) {
				console.log(response);
			});
		}


		facebook.prototype.login = function(action){
			FB.login(function(response){
				if (response.authResponse) {
					accessToken = response.authResponse.accessToken;
					getFacebookSession();
					if(action){
						action(response);
					}
				}
			}, {
				scope: 'publish_stream,email'
			});
		};

		facebook.prototype.logout = function(){
			FB.logout(function(response) {
				accessToken = null;
			});
		};


		facebook.prototype.share = function(){
			FB.ui({
				method: 'feed',
				name: 'Biko',
				link: window.location.href,
				picture: $('meta[property="og:image"]').attr('content'),
				caption: '',
				description: $('meta[name=description]').attr("content")
			});
		};

		return new facebook();
	}();

}(window, window.Biko));