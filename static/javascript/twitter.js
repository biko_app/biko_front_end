;(function(window, scope){

	'use strict';

	scope.Twitter = function(){ 
		
		/** @constructor */
		function twitter(){
		}


		twitter.prototype.share = function(){
			window.open("https://twitter.com/share?url="+encodeURIComponent(window.location));
		};

		return new twitter();
	}();

}(window, window.Biko));