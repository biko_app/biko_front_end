;(function(window, scope, $){

	'use strict';

	/**
	 * This class contains all specific functionality for the Register
	 */
	scope.Register = function(){

		var loginButton = $("#login-facebook"),
			registerButton = $("#register-facebook"),
			accessToken = $("#access_token");

		function register(){
			addEvents();
		}


		function addEvents(){
			loginButton.on("click", function(){
				window.Biko.Facebook.login(function(response){
					accessToken.val(response.authResponse.accessToken);
					$("form").submit();
				});
			});
			registerButton.on("click", function(){
				window.Biko.Facebook.login(function(response){
					accessToken.val(response.authResponse.accessToken);
					$("form").submit();
				});
			});
		}

		return new register();
	}();

}(window, window.Biko, jQuery));