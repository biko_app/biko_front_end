;(function(window, scope, $){

	'use strict';

	/**
	 * This class contains all specific functionality for the landing
	 */
	scope.Landing = function(){

		var hero = $("#page-hero"),
			header = $("#top-bar"),
			phone = $("#phone-mockup"),
			map = $("#map"),
			interactiveMap,
			animableObjects = $(".animable-object"),
			initialScrollTop = (phone.size()>0)?phone.offset().top:0,
			isMobil = (/android|webos|iphone|ipad|ipod|blackberry|iemobile|opera mini/i.test(navigator.userAgent.toLowerCase()));

		function adjustHero(){
			var height = $(window).height() + header.height();
			if(height<400){
				height = 400;
			}
			if (isMobil){
				height = height - 100;
			}
			hero.css({
				"height" : height
			});
		}

		function subscribe(){
			var modal;
			$("#subscribe-form").submit(function(){
				var emailField = $(this.email);
				if(/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test($(this.email).val())){
					emailField.val("");
					modal = window.Biko.createModal($("#subscribe_ok"));
					$.ajax(this.action,{
						'data': '{"email" : "'+emailField.val()+'"}',
						'type': 'POST',
						'processData': false,
						'contentType': 'application/json'
					}).done(function() {
					});
				}
				return false;
			});
		}

		function createMaker(type, color, latitude, longitude, icon){
			if(!type){
				type = "tienda";
			}
			if(!color){
				color = "#2bd2b6";
			}
			return new RichMarker({
				position: new google.maps.LatLng(latitude, longitude),
				map: interactiveMap,
				animation: google.maps.Animation.DROP,
				draggable: false,
				content: '<div class="mapa-pin">\
							<span class="icon-pin" style="color: '+color+';"><img src="/static/icons/places/'+icon+'"></span>\
						</div>'
			});
		}

		function adjustMap(){
			var zoom = 17;
			if(interactiveMap){
				if($(window).width() < 490){
					zoom = 16;
				}
				interactiveMap.setZoom(zoom);
			}
		}

		function initMap() {
			if($("#map").size()<1){
				return;
			}
			var zoom = 17;
			if($(window).width() < 490){
				zoom = 16;
			}
			var options = {
				zoom: zoom,
				disableDefaultUI: true,
				draggable: false,
				scrollwheel: false,
				streetViewControl: false,
				disableDoubleClickZoom: true,
				mapTypeId: google.maps.MapTypeId.ROADMAP,
				center: new google.maps.LatLng(4.675303, -74.058537)
			};
			interactiveMap = new google.maps.Map($("#map").get(0), options);
			$.getJSON(map.data("service"), function(data) {
				var i = 0;
				for (; i<data["home"].length; i++){
					createMaker(data["home"][i]["type"], data["home"][i]["color"], 
						data["home"][i]["location"]["latitude"], data["home"][i]["location"]["longitude"], data["home"][i]["icon"]);
				}
			});

			$("#more-info-landing").on("click", function(event){
				event.preventDefault();

				$('html, body').stop().animate({
					'scrollTop': $("#landing-first-section").offset().top
				}, 900, 'swing', null);
			})
		}

		function onScroll(event){
			if(isMobil || $(document.body).width()<600){
				return;
			}
			var scrollTop = $(window).scrollTop(),
				phonePos = 0;
			if(scrollTop >= 336){
				if(!header.hasClass("small")){
					header.addClass("small");
				}
			} else {
				if(header.hasClass("small")){
					header.removeClass("small");
				}
			}
			if(scrollTop >= initialScrollTop - 190){
				phonePos = scrollTop - (initialScrollTop - 190);
				if(	phonePos >= 670){
					phonePos = 670;
				}
			} else {
				phonePos = 0;
			}
			phone.find(".first-screen").css("transform", "translate3d(-"+((phonePos*100)/670)+"%, 0, 0)");
			phone.css("transform", "translate3d(0, "+phonePos+"px, 0)");
		}


		function onResize(event){
			initialScrollTop = (phone.size()>0)?phone.offset().top:0;
			adjustHero();
			adjustMap();
		}

		function landing(){
			setTimeout(function(){
				animableObjects.addClass("show");
			}, 300);
			$(window).scroll(onScroll);
			$(window).resize(onResize);
			adjustHero();
			initMap();
			subscribe();
		}

		return new landing();
	}();

}(window, window.Biko, jQuery));